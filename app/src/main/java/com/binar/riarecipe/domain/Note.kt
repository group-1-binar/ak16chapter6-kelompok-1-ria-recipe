package com.binar.riarecipe.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Note(
    val id: Int? = null,
    var noteTitle: String? = null,
    var notes: String? = null,
    var photoFilename: String? = null,
    var createdAt: Long? = null
): Parcelable