package com.binar.riarecipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.binar.riarecipe.databinding.FragmentPhotoPickerSheetBinding
import com.binar.riarecipe.presentation.PhotoPickerCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class PhotoPickerSheetFragment(val photoPickerCallback: PhotoPickerCallback) : BottomSheetDialogFragment() {

    lateinit var binding: FragmentPhotoPickerSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPhotoPickerSheetBinding.inflate(inflater, container, false)
        val view = binding.root
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonAction()
    }

    private fun buttonAction() {
        binding.apply {
            btnGallery.setOnClickListener {
                photoPickerCallback.onPhotoPickerMethodSelected(0)
                dismiss()
            }
            btnCamera.setOnClickListener {
                photoPickerCallback.onPhotoPickerMethodSelected(1)
                dismiss()
            }
        }
    }
}