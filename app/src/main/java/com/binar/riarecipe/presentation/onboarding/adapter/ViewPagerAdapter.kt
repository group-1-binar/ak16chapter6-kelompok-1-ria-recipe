package com.binar.riarecipe.presentation.onboarding.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.binar.riarecipe.presentation.onboarding.content.FirstIntroductionFragment
import com.binar.riarecipe.presentation.onboarding.content.FourthIntroductionFragment
import com.binar.riarecipe.presentation.onboarding.content.SecondIntroductionFragment
import com.binar.riarecipe.presentation.onboarding.content.ThirdIntroductionFragment

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private val fragmentData = listOf(
        FirstIntroductionFragment.newInstance(),
        SecondIntroductionFragment.newInstance(),
        ThirdIntroductionFragment.newInstance(),
        FourthIntroductionFragment.newInstance()
    )

    override fun getItemCount(): Int {
        return fragmentData.size
    }

    override fun createFragment(position: Int): Fragment = fragmentData[position]
}