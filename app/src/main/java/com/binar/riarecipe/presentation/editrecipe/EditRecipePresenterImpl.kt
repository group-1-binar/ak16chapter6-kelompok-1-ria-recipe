package com.binar.riarecipe.presentation.editrecipe

import android.app.Activity
import com.binar.riarecipe.data.db.AppDb
import com.binar.riarecipe.data.entity.NoteEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditRecipePresenterImpl(private val view: EditRecipeView): EditRecipePresenter {
    lateinit var appDb: AppDb
    override fun setUpDatabase() {
        appDb = AppDb.getDatabase(view as Activity)
    }

    override fun saveNote(note:NoteEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            appDb.noteDao().update(note)
            withContext(Dispatchers.Main){view.backToDetailPage()}
        }
    }

    override fun deleteNote(note: NoteEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            appDb.noteDao().delete(note)
        }
    }
}