package com.binar.riarecipe.presentation.mainpage

interface MainPagePresenter {

    fun setupDatabase()
    fun fetchData()
    fun searchNote(title: String)
}