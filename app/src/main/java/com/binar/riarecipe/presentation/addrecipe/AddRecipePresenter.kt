package com.binar.riarecipe.presentation.addrecipe

import com.binar.riarecipe.data.entity.NoteEntity

interface AddRecipePresenter {
    fun setUpDatabase()
    fun saveData(note: NoteEntity)
}