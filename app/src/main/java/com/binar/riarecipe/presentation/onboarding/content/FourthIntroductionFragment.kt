package com.binar.riarecipe.presentation.onboarding.content

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.binar.riarecipe.R
import com.binar.riarecipe.databinding.FragmentFourthIntroductionBinding
import com.binar.riarecipe.presentation.BaseFragment

class FourthIntroductionFragment : BaseFragment<FragmentFourthIntroductionBinding>() {

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFourthIntroductionBinding =
        FragmentFourthIntroductionBinding.inflate(inflater, container, false)

    companion object {
        @JvmStatic
        fun newInstance() =
            FourthIntroductionFragment()
    }
}