package com.binar.riarecipe.presentation.mainpage

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.binar.riarecipe.databinding.ListItemRecipeBinding
import com.binar.riarecipe.domain.Note
import com.binar.riarecipe.presentation.detailrecipe.DetailRecipe
import com.binar.riarecipe.presentation.editrecipe.EditRecipeActivity

class NotesAdapter(val context: Context) : RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {
    var notes = mutableListOf<Note>()

    class NotesViewHolder(val binding: ListItemRecipeBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val view = ListItemRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotesViewHolder(view)
    }

    override fun getItemCount(): Int = notes.size

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val currentItem = notes[position]
        holder.binding.apply {
            tvRecipeTitle.text = currentItem.noteTitle
            item.setOnClickListener {
                val intent = Intent(context, DetailRecipe::class.java)
                intent.putExtra("note", currentItem)
                context.startActivity(intent)
            }
        }
    }

    fun setListData(notes: List<Note>) {
        this.notes = notes.toMutableList()
        notifyDataSetChanged()
    }
}