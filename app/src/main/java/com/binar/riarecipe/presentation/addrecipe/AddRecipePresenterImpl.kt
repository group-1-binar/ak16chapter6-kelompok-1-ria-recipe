package com.binar.riarecipe.presentation.addrecipe

import android.app.Activity
import com.binar.riarecipe.data.db.AppDb
import com.binar.riarecipe.data.entity.NoteEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddRecipePresenterImpl(private val addRecipeView: AddRecipeView): AddRecipePresenter {
    lateinit var appDb: AppDb
    override fun setUpDatabase() {
        appDb = AppDb.getDatabase(addRecipeView as Activity)
    }

    override fun saveData(note: NoteEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            appDb.noteDao().insertAll(note)
        }
    }
}