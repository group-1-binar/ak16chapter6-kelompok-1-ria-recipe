package com.binar.riarecipe.presentation.editrecipe

interface EditRecipeView {
    fun backToDetailPage()
}