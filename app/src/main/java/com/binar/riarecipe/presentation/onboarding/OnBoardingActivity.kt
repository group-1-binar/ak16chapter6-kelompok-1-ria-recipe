package com.binar.riarecipe.presentation.onboarding

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.binar.riarecipe.R
import com.binar.riarecipe.databinding.ActivityOnBoardingBinding
import com.binar.riarecipe.presentation.mainpage.MainPageActivity
import com.binar.riarecipe.presentation.onboarding.adapter.ViewPagerAdapter
import com.binar.riarecipe.presentation.splashscreen.SplashScreenActivity
import com.google.android.material.tabs.TabLayoutMediator

class OnBoardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingBinding
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        checkPermission()

        setUpSharedPref()
        setUpAction()
        setupView()
    }

    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= 29) {
            val readStoragePermissionCheck =
                checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            val writeStoragePermissionCheck =
                checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val cameraPermissionCheck =
                checkSelfPermission(android.Manifest.permission.CAMERA)

            if (readStoragePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                writeStoragePermissionCheck == PackageManager.PERMISSION_GRANTED &&
                cameraPermissionCheck == PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
            }

            if (readStoragePermissionCheck != PackageManager.PERMISSION_GRANTED ||
                writeStoragePermissionCheck != PackageManager.PERMISSION_GRANTED ||
                cameraPermissionCheck != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(this, "Some Permission Denied", Toast.LENGTH_LONG).show()
                requestPermission()
            }
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= 29) {
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ), SplashScreenActivity.REQUEST_PERMISSION_CODE
            )
        }
    }

    private fun setUpAction() {
        binding.apply {
            buttonFinish.setOnClickListener {
                // Save status first opened app jadi false
                sharedPref.edit().putBoolean(getString(R.string.key_is_first_open_app), false).apply()
                // Pindah ke main page activity
                val intent = Intent(this@OnBoardingActivity, MainPageActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }
    }

    private fun setUpSharedPref() {
        sharedPref = this.getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE)
    }

    private fun setupView() {
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, lifecycle)
        binding.apply {
            viewPagerIntro.adapter = viewPagerAdapter
            TabLayoutMediator(tabLayoutIntro, viewPagerIntro) { _, _ -> }.attach()
            setupOnSlideChanged()
        }
    }

    private fun setupOnSlideChanged() {
        binding.apply {
            viewPagerIntro.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> buttonFinish.visibility = View.INVISIBLE
                        1 -> buttonFinish.visibility = View.INVISIBLE
                        2 -> buttonFinish.visibility = View.INVISIBLE
                        3 -> {
                            buttonFinish.visibility = View.VISIBLE
                        }
                    }

                    super.onPageSelected(position)
                }
            })
        }
    }
}