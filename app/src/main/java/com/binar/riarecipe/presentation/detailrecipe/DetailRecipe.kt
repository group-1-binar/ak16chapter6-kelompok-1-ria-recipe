package com.binar.riarecipe.presentation.detailrecipe

import android.content.Context
import android.content.ContextWrapper
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.binar.riarecipe.databinding.ActivityDetailRecipeBinding
import com.binar.riarecipe.domain.Note
import com.binar.riarecipe.mapper.Mapper.mapToEntity
import com.binar.riarecipe.presentation.editrecipe.EditRecipeActivity
import com.bumptech.glide.Glide
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException


class DetailRecipe : AppCompatActivity(), DetailRecipeView {

    lateinit var binding: ActivityDetailRecipeBinding
    lateinit var presenter: DetailRecipePresenter
    var note: Note? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailRecipeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpPresenter()
        getIntentData()
        buttonAction()
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchDetailNote(note?.id!!)
    }

    private fun setUpPresenter() {
        presenter = DetailRecipePresenterImpl(this)
        presenter.setUpDatabase()
    }

    private fun setUpView() {
        binding.apply {
            tvTitle.text = note?.noteTitle
            tvRecipe.text = note?.notes

            loadImageFromStorage(note?.photoFilename?:"")
            if (note?.photoFilename !== null){
                ivFoto.visibility = View.VISIBLE
            }
        }
    }


    private fun buttonAction(){
        binding.apply {
            ivEdit.setOnClickListener {
                val intent = Intent(this@DetailRecipe, EditRecipeActivity::class.java)
                intent.putExtra("note", note)
                startActivity(intent)
            }
            ivDelete.setOnClickListener {
                // delete
                AlertDialog.Builder(this@DetailRecipe)
                    .setTitle("Konfirmasi")
                    .setMessage("Apakah anda yakin ingin menghapus catatan ini?")
                    .setPositiveButton("Iya", DialogInterface.OnClickListener { dialog, which ->
                        note?.mapToEntity()?.let { it1 ->
                            presenter.deleteNote(it1)
                            backToMainPage()
                        }
                    })
                    .setNegativeButton("Tidak", DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                    })
                    .show()
            }
            ivBackButton.setOnClickListener {
                backToMainPage()
            }
        }
    }

    private fun loadImageFromStorage(path: String) {
        try {
            val cw = ContextWrapper(applicationContext)
            Log.d("PHOTO", path)
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            val f = File(directory, path)
            val b = BitmapFactory.decodeStream(FileInputStream(f))
            Glide.with(this).load(b).into(binding.ivFoto)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun getIntentData() {
        setNewNote(intent.extras?.getParcelable<Note>("note"))
    }

    override fun backToMainPage() {
        finish()
    }

    override fun setNewNote(note: Note?) {
        this.note = note
        setUpView()
    }
}