package com.binar.riarecipe.presentation.splashscreen

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.binar.riarecipe.R
import com.binar.riarecipe.presentation.mainpage.MainPageActivity
import com.binar.riarecipe.presentation.onboarding.OnBoardingActivity
import kotlinx.coroutines.delay
import kotlin.properties.Delegates

class SplashScreenActivity : AppCompatActivity() {

    lateinit var sharedPref: SharedPreferences
    var isFirstOpenApp by Delegates.notNull<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        lifecycleScope.launchWhenCreated {
            setUpSharedPref()
            isFirstOpenApp = getIsFirstOpenApp()
            delay(3000)
            val intent =
                if (isFirstOpenApp) {
                    Intent(this@SplashScreenActivity, OnBoardingActivity::class.java)
                } else {
                    Intent(this@SplashScreenActivity, MainPageActivity::class.java)
                }
            startActivity(intent)
            finishAffinity()
        }
    }

    private fun getIsFirstOpenApp(): Boolean {
        return sharedPref.getBoolean(getString(R.string.key_is_first_open_app), true)
    }

    private fun setUpSharedPref() {
        sharedPref =
            this.getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE)
    }

    companion object {
        const val REQUEST_PERMISSION_CODE = 201
    }
}