package com.binar.riarecipe.presentation.editrecipe

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import com.binar.riarecipe.PhotoPickerSheetFragment
import com.binar.riarecipe.databinding.ActivityEditRecipeBinding
import com.binar.riarecipe.domain.Note
import com.binar.riarecipe.mapper.Mapper.mapToEntity
import com.binar.riarecipe.presentation.PhotoPickerCallback
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import java.io.*
import java.util.*

class EditRecipeActivity : AppCompatActivity(), EditRecipeView {

    lateinit var binding: ActivityEditRecipeBinding
    lateinit var presenter: EditRecipePresenter

    var note: Note? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditRecipeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpPresenter()
        getIntentData()
        setUpView()
        buttonAction()
    }

    private fun buttonAction() {
        binding.apply {
            btnSave.setOnClickListener {
                if (note?.notes.isNullOrEmpty() || note?.noteTitle.isNullOrEmpty()) {
                    Snackbar.make(
                        binding.root,
                        "Silakan lengkapi catatan terlebih dahulu",
                        Snackbar.LENGTH_LONG
                    ).show()
                    return@setOnClickListener
                }
                presenter.saveNote(note?.mapToEntity()!!)
            }

            ivBackButton.setOnClickListener {
                backToDetailPage()
            }
            ivFoto.setOnClickListener {
                PhotoPickerSheetFragment(object : PhotoPickerCallback {
                    override fun onPhotoPickerMethodSelected(value: Int) {
                        when (value) {
                            PhotoPickerCallback.PICK_BY_GALLERY -> {
                                showImagePicker()
                            }
                            PhotoPickerCallback.PICK_BY_CAMERA -> {
                                showCameraPicker()
                            }
                        }
                    }
                }).show(supportFragmentManager, "PHOTO_PICKER")
            }
        }
    }

    private fun showCameraPicker() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 150)
    }

    private fun showImagePicker(capture: Boolean = false) {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, 20)
    }


    private fun setUpPresenter() {
        presenter = EditRecipePresenterImpl(this)
        presenter.setUpDatabase()
    }

    private fun setUpView() {
        binding.apply {
            etTitle.setText(note?.noteTitle)
            etResep.setText(note?.notes)
            // set Image view

            etTitle.doOnTextChanged { text, start, before, count ->
                note?.noteTitle = text.toString()
            }
            etResep.doOnTextChanged { text, start, before, count ->
                note?.notes = text.toString()
            }
            loadImageFromStorage(note?.photoFilename)
        }
    }

    private fun saveToInternalStorage(bitmapImage: Bitmap): String? {
        val cw = ContextWrapper(applicationContext)
        // path to /data/data/yourapp/app_data/imageDir
        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
        // Create imageDir
        val filename = "${Date().time}.jpg"
        val mypath = File(directory, filename)
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        Log.d("Absolute", mypath.absolutePath)
        Log.d("tostring", mypath.toString())
        return filename
    }

    private fun loadImageFromStorage(path: String?) {
        if (path == null) return
        try {
            val cw = ContextWrapper(applicationContext)
            Log.d("PHOTO", path)
            val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
            val f = File(directory, path)
            val b = BitmapFactory.decodeStream(FileInputStream(f))
            Glide.with(this).load(b).into(binding.ivFoto)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 20) {
            if (resultCode === RESULT_OK) {
                try {
                    val imageUri = data!!.data
                    val imageStream: InputStream? = contentResolver.openInputStream(imageUri!!)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val photoFilename = saveToInternalStorage(selectedImage)
                    binding.ivFoto.setImageBitmap(selectedImage)
                    note?.photoFilename = photoFilename!!
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show()
            }
        }  else if (requestCode == 150){
            val bitmap = data?.extras?.get("data") as Bitmap

            val filename = saveToInternalStorage(bitmap)
            note?.photoFilename = filename
            binding.ivFoto.setImageBitmap(bitmap)
//            Toast.makeText(this, bitmap.toString(), Toast.LENGTH_LONG).show()
        }
    }

    private fun getIntentData() {
        note = intent.extras?.getParcelable<Note>("note")
    }

    override fun backToDetailPage() {
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}