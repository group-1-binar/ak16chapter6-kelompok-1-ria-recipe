package com.binar.riarecipe.presentation.editrecipe

import com.binar.riarecipe.data.entity.NoteEntity

interface EditRecipePresenter {
    fun setUpDatabase()
    fun saveNote(note:NoteEntity)
    fun deleteNote(note: NoteEntity)
}