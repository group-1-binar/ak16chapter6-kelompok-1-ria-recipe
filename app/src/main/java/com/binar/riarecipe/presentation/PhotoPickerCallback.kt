package com.binar.riarecipe.presentation

interface PhotoPickerCallback {
    companion object{
        const val PICK_BY_GALLERY = 0
        const val PICK_BY_CAMERA = 1
    }
    fun onPhotoPickerMethodSelected(value: Int)
}