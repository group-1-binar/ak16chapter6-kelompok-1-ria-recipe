package com.binar.riarecipe.presentation.detailrecipe

import com.binar.riarecipe.data.entity.NoteEntity

interface DetailRecipePresenter {
    fun setUpDatabase()
    fun deleteNote(note: NoteEntity)
    fun fetchDetailNote(noteId: Int)
}