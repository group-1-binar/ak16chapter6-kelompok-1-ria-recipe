package com.binar.riarecipe.presentation.mainpage

import android.app.Activity
import com.binar.riarecipe.data.db.AppDb
import com.binar.riarecipe.mapper.Mapper.mapToListDomainModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainPagePresenterImpl(private val view: MainPageView) : MainPagePresenter {

    lateinit var appDb: AppDb

    override fun setupDatabase() {
        appDb = AppDb.getDatabase(view as Activity)
    }

    override fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val listNotes = appDb.noteDao().findAll().mapToListDomainModel()
            withContext(Dispatchers.Main) { view.setListItem(listNotes) }
        }
    }

    override fun searchNote(title: String) {
        CoroutineScope(Dispatchers.IO).launch {
            if (title !== ""){
                val listNotes = appDb.noteDao().findByTitle(title).mapToListDomainModel()
                if (listNotes.isEmpty()){
                    withContext(Dispatchers.Main){ view.showSnackBar("Data tidak ditemukan.")}
                }
                withContext(Dispatchers.Main) { view.setListItem(listNotes) }
            } else {
                fetchData()
            }
        }
    }
}