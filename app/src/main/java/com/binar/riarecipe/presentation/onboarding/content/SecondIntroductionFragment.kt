package com.binar.riarecipe.presentation.onboarding.content

import android.view.LayoutInflater
import android.view.ViewGroup
import com.binar.riarecipe.databinding.FragmentSecondIntroductionBinding
import com.binar.riarecipe.presentation.BaseFragment

class SecondIntroductionFragment : BaseFragment<FragmentSecondIntroductionBinding>() {

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSecondIntroductionBinding = FragmentSecondIntroductionBinding.inflate(inflater, container, false)

    companion object {
        @JvmStatic
        fun newInstance() =
            SecondIntroductionFragment()
    }

}