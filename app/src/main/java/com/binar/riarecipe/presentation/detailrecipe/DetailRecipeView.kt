package com.binar.riarecipe.presentation.detailrecipe

import com.binar.riarecipe.domain.Note

interface DetailRecipeView {
    fun backToMainPage()
    fun setNewNote(note:Note?)
}