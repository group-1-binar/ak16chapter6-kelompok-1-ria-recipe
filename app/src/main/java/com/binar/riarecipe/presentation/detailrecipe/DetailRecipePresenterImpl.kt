package com.binar.riarecipe.presentation.detailrecipe

import android.app.Activity
import com.binar.riarecipe.data.db.AppDb
import com.binar.riarecipe.data.entity.NoteEntity
import com.binar.riarecipe.mapper.Mapper.mapToDomainModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailRecipePresenterImpl(private val view: DetailRecipeView): DetailRecipePresenter {
    lateinit var appDb: AppDb
    override fun setUpDatabase() {
        appDb = AppDb.getDatabase(view as Activity)
    }

    override fun deleteNote(note: NoteEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            appDb.noteDao().delete(note)
        }
    }

    override fun fetchDetailNote(noteId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val note = appDb.noteDao().findById(noteId)
            withContext(Dispatchers.Main){view.setNewNote(note.mapToDomainModel())}
        }
    }
}