package com.binar.riarecipe.presentation.onboarding.content

import android.view.LayoutInflater
import android.view.ViewGroup
import com.binar.riarecipe.databinding.FragmentFirstIntroductionBinding
import com.binar.riarecipe.presentation.BaseFragment

class FirstIntroductionFragment : BaseFragment<FragmentFirstIntroductionBinding>() {

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFirstIntroductionBinding =
        FragmentFirstIntroductionBinding.inflate(inflater, container, false)

    companion object {
        fun newInstance(): FirstIntroductionFragment {
            return FirstIntroductionFragment()
        }
    }

}