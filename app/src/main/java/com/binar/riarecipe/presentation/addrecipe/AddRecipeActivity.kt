package com.binar.riarecipe.presentation.addrecipe

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.binar.riarecipe.PhotoPickerSheetFragment
import com.binar.riarecipe.databinding.ActivityAddRecipeBinding
import com.binar.riarecipe.domain.Note
import com.binar.riarecipe.mapper.Mapper.mapToEntity
import com.binar.riarecipe.presentation.PhotoPickerCallback
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class AddRecipeActivity : AppCompatActivity(), AddRecipeView {

    lateinit var binding: ActivityAddRecipeBinding
    lateinit var presenter: AddRecipePresenter
    lateinit var currentPhotoPath: String

    var photoUri: String? = null
    var note: Note = Note()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddRecipeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpPresenter()
        buttonAction()
    }

    private fun setUpPresenter() {
        presenter = AddRecipePresenterImpl(this)
        presenter.setUpDatabase()
    }

    private fun buttonAction() {
        binding.apply {
            btnSave.setOnClickListener {
                note.notes = etResep.text.toString()
                note.noteTitle = etTitle.text.toString()
                note.photoFilename = photoUri
                note.createdAt = Date().time
                val noteEntity = note.mapToEntity()
                if (note.noteTitle.isNullOrEmpty() || note.notes.isNullOrEmpty()){
                    Snackbar.make(binding.root, "Silakan lengkapi catatan terlebih dahulu", Snackbar.LENGTH_LONG).show()
                } else {
                    presenter.saveData(noteEntity)
                    backToMainPage()
                }
            }
            ivBackButton.setOnClickListener {
                backToMainPage()
            }
            ivFoto.setOnClickListener {
                PhotoPickerSheetFragment(object :PhotoPickerCallback{
                    override fun onPhotoPickerMethodSelected(value: Int) {
                        when(value){
                            PhotoPickerCallback.PICK_BY_GALLERY -> {showImagePicker()}
                            PhotoPickerCallback.PICK_BY_CAMERA -> {showCameraPicker()}
                        }
                    }
                }).show(supportFragmentManager, "PHOTO_PICKER")
            }
        }
    }

    private fun showCameraPicker(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 150)
    }

    private fun showImagePicker(capture: Boolean = false) {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, 20)
    }

    private fun saveToInternalStorage(bitmapImage: Bitmap): String? {
        val cw = ContextWrapper(applicationContext)
        // path to /data/data/yourapp/app_data/imageDir
        val directory: File = cw.getDir("imageDir", Context.MODE_PRIVATE)
        // Create imageDir
        val filename = "${Date().time}.jpg"
        val mypath = File(directory, filename)
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        Log.d("Absolute", mypath.absolutePath)
        Log.d("tostring", mypath.toString())
        return filename
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 20){
            if (resultCode === RESULT_OK) {
                try {
                    val imageUri = data!!.data
                    val imageStream: InputStream? = contentResolver.openInputStream(imageUri!!)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val filename = saveToInternalStorage(selectedImage)
                    binding.ivFoto.setImageBitmap(selectedImage)
                    photoUri = filename!!
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == 150){
            val bitmap = data?.extras?.get("data") as Bitmap

            val filename = saveToInternalStorage(bitmap)
            photoUri = filename
            binding.ivFoto.setImageBitmap(bitmap)
//            Toast.makeText(this, bitmap.toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun backToMainPage() {
        finish()
    }


}