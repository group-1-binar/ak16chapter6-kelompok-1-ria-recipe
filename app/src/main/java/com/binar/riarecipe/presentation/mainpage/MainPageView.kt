package com.binar.riarecipe.presentation.mainpage

import com.binar.riarecipe.domain.Note

interface MainPageView {
    fun setListItem(notes: List<Note>)
    fun showSnackBar(text: String)
}