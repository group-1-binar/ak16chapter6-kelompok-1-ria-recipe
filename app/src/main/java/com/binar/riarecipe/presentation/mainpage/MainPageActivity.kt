package com.binar.riarecipe.presentation.mainpage

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.binar.riarecipe.databinding.ActivityMainPageBinding
import com.binar.riarecipe.domain.Note
import com.binar.riarecipe.presentation.addrecipe.AddRecipeActivity
import com.google.android.material.snackbar.Snackbar

class MainPageActivity : AppCompatActivity(), MainPageView {

    lateinit var presenter: MainPagePresenter
    lateinit var rvAdapter: NotesAdapter
    lateinit var binding: ActivityMainPageBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainPageBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setUpPresenter()
        setUpAdapter()
        setUpView()
        buttonAction()
    }

    private fun setUpView() {
        binding.apply {
            etSearch.doOnTextChanged { text, start, before, count ->
                presenter.searchNote(text.toString())
            }
        }
    }

    private fun buttonAction() {
        binding.apply {
            fabAddNote.setOnClickListener {
                val intent = Intent(this@MainPageActivity, AddRecipeActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchData()
    }

    private fun setUpPresenter() {
        presenter = MainPagePresenterImpl(this)
        presenter.setupDatabase()
    }

    private fun setUpAdapter() {
        rvAdapter = NotesAdapter(this)
        val rvLayout = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.apply {
            rvNotes.adapter = rvAdapter
            rvNotes.layoutManager = rvLayout
        }
    }

    override fun setListItem(notes: List<Note>) {
        if (notes.isEmpty()) {
            binding.tvEmptyNote.visibility = View.VISIBLE
        } else {
            binding.tvEmptyNote.visibility = View.GONE
        }

        rvAdapter.setListData(notes)

    }

    override fun showSnackBar(text: String) {
        Snackbar.make(binding.root, text, Snackbar.LENGTH_LONG).show()
    }
}