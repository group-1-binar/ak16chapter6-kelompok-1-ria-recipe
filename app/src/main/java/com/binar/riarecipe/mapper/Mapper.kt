package com.binar.riarecipe.mapper

import com.binar.riarecipe.data.entity.NoteEntity
import com.binar.riarecipe.domain.Note

object Mapper {

    fun NoteEntity.mapToDomainModel() : Note =
        Note(
            id = this.id,
            noteTitle = this.noteTitle,
            notes = this.notes,
            photoFilename = this.photoFilename,
            createdAt = this.createdAt
        )

    fun List<NoteEntity>.mapToListDomainModel(): List<Note> =
        this.map {
            Note(
                id = it.id,
                noteTitle = it.noteTitle,
                notes = it.notes,
                photoFilename = it.photoFilename,
                createdAt = it.createdAt
            )
        }

    fun Note.mapToEntity(): NoteEntity =
        NoteEntity(
            id = this.id?:0,
            noteTitle = this.noteTitle,
            notes = this.notes,
            photoFilename = this.photoFilename,
            createdAt = this.createdAt
        )
}