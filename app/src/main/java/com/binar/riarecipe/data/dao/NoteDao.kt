package com.binar.riarecipe.data.dao

import androidx.room.*
import com.binar.riarecipe.data.entity.NoteEntity


@Dao
interface NoteDao {
    // Menampilkan seluruh data di mainpage
    @Query("SELECT * FROM note")
    suspend fun findAll(): List<NoteEntity>

    // Menampilkan data di halaman detailpage
    @Query("SELECT * FROM note WHERE id = :noteId")
    suspend fun findById(noteId: Int): NoteEntity

    // Menampilkan data hasil fitur pencarian
    @Query("SELECT * FROM note WHERE title like '%' || :title || '%'")
    fun findByTitle(title: String): List<NoteEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg noteEntities: NoteEntity)

    @Delete
    fun delete(noteEntity: NoteEntity)

    @Update
    suspend fun update(noteEntity: NoteEntity)
}