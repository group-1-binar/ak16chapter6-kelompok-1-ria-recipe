package com.binar.riarecipe.data.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity("note")
@kotlinx.parcelize.Parcelize
data class NoteEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "title") val noteTitle: String?,
    @ColumnInfo(name = "notes") val notes: String?,
    @ColumnInfo(name = "photo_filename") val photoFilename: String?,
    @ColumnInfo(name = "created_at") val createdAt: Long?
): Parcelable